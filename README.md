This is Stamp Detector using FastAI  + CLI

You can test it on the dataset: http://madm.dfki.de/downloads-ds-staver (18GByte!)

```bash
git clone git@gitlab.com:karel.durkota/stamp-detector.git
cd stamp-detector
python3 -m venv venv
. venv/bin/acticate
pip install -e .
```

Download dataset
```bash
wget https://madm.dfki.de/files/downloads/StaVer.tar.gz
tar zxvf StaVer.tar.gz
```

Train detector on the data:
```bash
train --images=./genuine/200dpi/scans --labels=./genuine/200dpi/ground-truth-pixel --save_to=model
```

See predictions:
```bash
predict --img=./genuine/200dpi/scans/stampDS-00001.png --model=model
```

To see the predicted bounding box and segmentation:
```bash
predict --img=./genuine/200dpi/scans/stampDS-00001.png --model=model --export_box=./boxes --export_seg=./segs
```

Predict on all files in directory (>400 files)
```bash
predict --img=./genuine/200dpi/scans/ --model=model --show_box --show_seg
```

Evaluate on files
```bash
predict --images=./genuine/200dpi/scans/ --labels=./genuine/200dpi/ground-truth-pixel/ --model=model 
```



