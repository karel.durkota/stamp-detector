import numpy as np

from stamp_detector.utils import get_iou, mask2bbox_from_bounds


def test_get_iou():
    bbox1 = {'x1': 0,
             'y1': 0,
             'x2': 40,
             'y2': 40}
    bbox2 = {'x1': 20,
             'y1': 20,
             'x2': 60,
             'y2': 60}

    intersection = 20 * 20
    union = 40 * 40 + 40 * 40 - 20 * 20
    expected_iou = intersection / union

    iou = get_iou(bbox1, bbox2)
    assert expected_iou == iou


def test_mask2bbox_from_bounds():
    mask = np.asarray([
        [0., 0., 0., 0., 0.],
        [0., 0.6, 0.8, 0.6, 0],
        [0., 0.6, 0.8, 0.8, 0],
        [0., 0.6, 0.8, 0.6, 0],
        [0., 0., 0., 0., 0.]
    ])
    bbox_5 = mask2bbox_from_bounds(mask, threshold=0.5)
    assert bbox_5['x1'] == 1
    assert bbox_5['y1'] == 1
    assert bbox_5['x2'] == 3
    assert bbox_5['y2'] == 3
    bbox_7 = mask2bbox_from_bounds(mask, threshold=0.7)
    assert bbox_7['x1'] == 2
    assert bbox_7['y1'] == 1
    assert bbox_7['x2'] == 3
    assert bbox_7['y2'] == 3
    bbox_9 = mask2bbox_from_bounds(mask, threshold=0.9)
    assert bbox_9 is None
