import numpy as np
from fastai.vision.core import PILMask


def get_iou(bb1, bb2):
    """
    Calculate the Intersection over Union (IoU) of two bounding boxes.

    Parameters
    ----------
    bb1 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    bb2 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x, y) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner

    Returns
    -------
    float
        in [0, 1]

    Source: https://stackoverflow.com/questions/25349178/calculating-percentage-of-bounding-box-overlap-for-image-detector-evaluation
    """
    assert bb1['x1'] < bb1['x2']
    assert bb1['y1'] < bb1['y2']
    assert bb2['x1'] < bb2['x2']
    assert bb2['y1'] < bb2['y2']

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1['x1'], bb2['x1'])
    y_top = max(bb1['y1'], bb2['y1'])
    x_right = min(bb1['x2'], bb2['x2'])
    y_bottom = min(bb1['y2'], bb2['y2'])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1['x2'] - bb1['x1']) * (bb1['y2'] - bb1['y1'])
    bb2_area = (bb2['x2'] - bb2['x1']) * (bb2['y2'] - bb2['y1'])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the intersection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou


def mask2bbox_from_bounds(b: np.array, threshold: float = 0.0):
    b = (b > threshold).astype(np.int)
    if np.all(b == 0):
        return None

    box = {'x1': b.max(axis=0).argmax(),
           'y1': b.max(axis=1).argmax(),
           'x2': b.shape[0] - b.max(axis=0)[::-1].argmax() - 1,
           'y2': b.shape[1] - b.max(axis=1)[::-1].argmax() - 1}
    if box['x1'] >= box['x2']:
        return None
    elif box['y1'] >= box['y2']:
        return None
    else:
        return box


def identity(x):
    return x


def get_msk(filename):
    """
    Grab a mask from a `filename` and adjust the pixels based on `pix2class`"
    """
    filename = filename.split(';')[1]
    msk = np.array(PILMask.create(filename))
    msk //= 255
    return PILMask.create(msk)


def _first(a):
    return a.split(';')[0]
