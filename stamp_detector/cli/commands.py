import logging

import click

from stamp_detector.stamp_detector import StampDetector


@click.group()
def main():
    """
    Stamp Detector command line utility.
    """
    pass


@main.command("train")
@click.option('--save_to', required=True, help="Path to save trained model.")
@click.option('--images', required=True, help="Directory to images.", type=click.Path(exists=True))
@click.option('--labels', required=True, help="Directory to labels. Make sure, that python sorted() function returns "
                                              "images and labels in the same order.", type=click.Path(exists=True))
def train(save_to, images, labels):
    """Simple program that greets NAME for a total of COUNT times."""
    model_inst = StampDetector()
    model_inst.train(images, labels)
    model_inst.export(save_to)


@main.command("evaluate")
@click.option('--model', default=None, help="Path to the model.")
@click.option('--images', required=True, help="Directory to images", type=click.Path(exists=True))
@click.option('--labels', required=True, help="Directory to labels", type=click.Path(exists=True))
def evaluate(model, images, labels):
    model_inst = StampDetector(model)
    metrics = model_inst.evaluate(images, labels)
    logging.info(str(metrics))


@main.command("predict")
@click.option('--model', default=None, help="Path to the model.", type=click.Path(exists=True))
@click.option('--images', required=True, help="Path to image or directory with images.", type=click.Path(exists=True))
@click.option('--export_bbox', default=None, help="Directory to save predicted bbox images.")
@click.option('--export_seg', default=None, help="Directory to save predicted segmentation images.")
def predict(model, images, export_bbox, export_seg):
    """Simple program that greets NAME for a total of COUNT times."""
    model_inst = StampDetector(model)
    model_inst.predict(images, export_bbox, export_seg)


if __name__ == '__main__':
    main()
