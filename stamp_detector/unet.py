import logging
from typing import Tuple, Dict, Optional, List

import numpy as np
import torch
from PIL import Image
from fastai.data.block import DataBlock
from fastai.data.core import DataLoaders
from fastai.data.transforms import get_image_files, RandomSplitter, Normalize
from fastai.learner import load_learner
from fastai.vision.augment import Resize
from fastai.vision.core import imagenet_stats
from fastai.vision.data import ImageBlock, MaskBlock
from fastai.vision.learner import unet_learner
from fastcore.xtras import Path
from torchvision.models import resnet34

from stamp_detector.utils import mask2bbox_from_bounds, get_iou, identity, get_msk, _first


class UNet:
    """
    This class uses FastAI unet model for stamp detection.
    """

    def __init__(self, config, model_path='model'):
        self.config = config
        self.model_path = model_path
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.model_img_size = config['MODEL'].getint('img_size')
        self.segmentation_threshold = self.config['MODEL'].getfloat('segmentation_threshold')
        self.codes = ['Background', 'Stamp']
        self.learn = None
        self.datablock = DataBlock(blocks=[ImageBlock, MaskBlock(self.codes)],
                                   get_items=identity,
                                   splitter=RandomSplitter(),
                                   get_x=_first,
                                   get_y=get_msk,
                                   item_tfms=[Resize(self.model_img_size, method='squish')],
                                   batch_tfms=[Normalize.from_stats(*imagenet_stats)])

    def predict(self, fnames: List[Path]) -> Tuple:
        """
        Predicts on images in the list `fnames`.

        param fnames: list of paths to images
        return: tuple boxes (bounding boxes) and segmentations of predictions
        """
        self._load_learner_if_needed()
        dl = self.learn.dls.test_dl(fnames)
        predictions, _ = self.learn.get_preds(dl=dl)
        predictions = predictions.numpy()

        # rescale back the boxes
        boxes = []
        segmentations = []
        for fname, pred in zip(fnames, predictions):
            image_size_box = self._convert_prediction_to_bbox(pred[0], fname)
            boxes.append(image_size_box)
            segmentations.append(pred)

        return boxes, segmentations

    def train(self, images: Path, labels: Path):
        """
        Method to train model from scratch.

        Files in `images` and `labels` are sorted by names. Files (image, label) must correspond to each other
        when sorted. If one contains less files, only subset will be used for training.

        param images: Path to directory with images
        param labels: Path to directory with labels (mapping/segmentation)
        """
        dls = self._build_img_lbl_dataloader(images, labels)

        self.learn = unet_learner(dls, resnet34)
        self.learn.fit(n_epoch=self.config['LEARN'].getint('epochs'),
                       lr=self.config['LEARN'].getfloat('lr'))

    def evaluate(self, images: Path, labels: Path) -> Dict[str, float]:
        """
        Computes segmentation loss and IoU of the model over `images` using `labels`
        """
        self._load_learner_if_needed()
        dls = self._build_img_lbl_dataloader(images, labels)

        preds, ys, seg_loss = self.learn.get_preds(dl=dls, with_loss=True)
        preds, ys, seg_loss = preds.numpy(), ys.numpy(), seg_loss.numpy()

        mean_iou = self._mean_iou(preds, ys)
        result = {'mean_segmentation_loss': np.mean(seg_loss),
                  'mean_iou': mean_iou}

        return result

    def _build_img_lbl_dataloader(self, images: Path, labels: Path) -> DataLoaders:
        fnames = sorted(get_image_files(images))
        lbl_names = sorted(get_image_files(labels))

        if len(fnames) != len(lbl_names):
            logging.warning(
                f'There are {len(fnames)} images and {len(lbl_names)} labels. First {min(len(fnames), len(lbl_names))}'
                f' will be taken.')

        pairs = [';'.join([str(a), str(b)]) for a, b in zip(sorted(fnames), sorted(lbl_names))]
        binary = self.datablock
        dls = binary.dataloaders(pairs, bs=self.config['DEFAULT'].getint('batch_size'),
                                 device=torch.device(self.device))
        return dls

    def _convert_prediction_to_bbox(self, pred: np.array,
                                    fname: Path = None,
                                    img_size: Tuple[int, int] = None) -> Optional[Dict[str, int]]:
        """
        Converts predictions into bounding boxes. Original image size is needed for it, which is either provided
        `img_size` or extracted from the image file `fname`.

        :param pred: 2D np.array of the prediction.
        :param fname: path to filename. Read only if img_size is not provided.
        :param img_size: tuple (w,h) of the image.
        :returns: Dictionary of bounding boxes. Keys: x1, x2, y1, y2.
        """
        if img_size is None:
            img = Image.open(fname)
            img_size = img.size

        # find maximal bounds
        box = mask2bbox_from_bounds(pred, threshold=self.config['MODEL'].getfloat('segmentation_threshold'))
        if box is None:
            return None

        relative_box = {a: b / self.model_img_size for a, b in box.items()}

        image_size_box = {'x1': int(relative_box['x1'] * img_size[0]),
                          'y1': int(relative_box['y1'] * img_size[1]),
                          'x2': int(relative_box['x2'] * img_size[0]),
                          'y2': int(relative_box['y2'] * img_size[1])}
        return image_size_box

    def _mean_iou(self, preds: np.array, ys: np.array) -> Optional[float]:
        """
        From predictions and segmentation mask extracts object bounding boxes and returns IoU score.
        """
        iou = []
        for pred, y in zip(preds, ys):
            true_bbox = mask2bbox_from_bounds(y, threshold=0.5)
            pred_bbox = mask2bbox_from_bounds(pred[0],
                                              threshold=self.config['MODEL'].getfloat('segmentation_threshold'))
            if true_bbox is None:
                logging.warning(f'No bounding box was detected on labeled image.')
            if pred_bbox is None:
                logging.warning(f'No bounding box was detected on prediction image.')

            if true_bbox is not None and pred_bbox is not None:
                iou.append(get_iou(true_bbox, pred_bbox))

        if len(iou) > 0:
            return float(np.mean(iou))
        else:
            return None

    def _load_learner_if_needed(self):
        self.learn = load_learner(self.model_path)
        self.learn.dls.to(device=self.device)
        self.learn.model.to(device=self.device)
