import configparser
import os

import numpy as np
from PIL import Image, ImageDraw as D
from fastai.data.transforms import get_image_files
from fastcore.xtras import Path

from stamp_detector.unet import UNet


class StampDetector:
    """
    Stamp detector Interface. Currently it uses FastAI's UNet for segmentation and then bboxing.
    """

    def __init__(self, model_path: str = None, config: str = 'config.cfg') -> None:
        self.model_path = model_path
        self.config = configparser.ConfigParser(allow_no_value=True)
        self.config.read(config)

        self.unet = None

    def predict(self, path: str, export_bbox: str = None, export_seg: str = None) -> None:
        """
        Predicts on image `path` or images in `path`.

        param path: str - path to images
        param export_bbox: str - path to save images with bounding boxes
        param export_seg: str - path to save images with segmentations
        """

        if os.path.isdir(path):
            fnames = get_image_files(path)
        elif os.path.isfile(path):
            fnames = [Path(path)]
        else:
            raise FileNotFoundError(path)

        if export_bbox and not os.path.exists(export_bbox):
            os.makedirs(export_bbox)
        if export_seg and not os.path.exists(export_seg):
            os.makedirs(export_seg)

        self.unet = UNet(self.config, model_path=self.model_path)
        bboxes, segmentations = self.unet.predict(fnames)

        for fname, box, seg in zip(fnames, bboxes, segmentations):
            if box is None:
                continue

            if export_bbox or export_seg:
                orig_img = Image.open(fname)

            if export_bbox:
                draw = D.Draw(orig_img)
                draw.rectangle((box['x1'], box['y1'], box['x2'], box['y2']), outline="green", width=5)
                orig_img.save(os.path.join(export_bbox, f'{fname.stem}_bbox{fname.suffix}'))

            if export_seg:
                p = seg[0]
                rescaled = (255.0 / p.max() * (p - p.min())).astype(np.uint8)
                img = Image.fromarray(rescaled)
                img = img.resize(orig_img.size)
                img.save(os.path.join(export_seg, f'{fname.stem}_seg{fname.suffix}'))

            # print the results.
            box['filename'] = str(fname)
            print(box)

    def train(self, images: str, labels: str):
        self.unet = UNet(self.config, self.model_path)
        self.unet.train(Path(images), Path(labels))

    def export(self, path):
        self.unet.learn.export(path)

    def evaluate(self, images: Path, labels: Path):
        unet = UNet(self.config, self.model_path)
        loss = unet.evaluate(images, labels)
        print(f'Segmentation loss: {loss}')
