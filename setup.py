import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="stamp_detector",
    version="0.0.1",
    author="Karel Durkota",
    author_email="karel.durkota@gmail.com",
    description="Project to train segmentation and bboxing using UNet from FastAI",
    long_description=long_description,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ],
    package_dir={"": "stamp_detector"},
    packages=setuptools.find_packages(where="stamp_detector"),
    python_requires=">=3.7",
    install_requires=[
        'fastai==2.7.9',
        'opencv-python==4.6.0.66',
        'scikit-image==0.19.3'
        'click~=8.0.4',
        'numpy~=1.21.6',
        'setuptools~=41.2.0',
        'torch~=1.12.1',
        'Pillow~=9.2.0',
        'fastcore~=1.5.27',
        'torchvision~=0.13.1',
        'tensorflow~=2.10.0',
        'tqdm~=4.64.1'
    ]
)
